<?php
/**
 * @author seirios-ls
 * 2021/6/7
 */

namespace BreakCode\BaseInterface;

interface Launch
{
    //验证
    public function identify(string $file, bool $base64 = false): Response;
}