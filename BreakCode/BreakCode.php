<?php

namespace BreakCode;

use BreakCode\BaseInterface\Response;

/**
 * @method Response identify($fileName, bool $base64  = false, $instance = null)
 */
class BreakCode
{

    private $instance;

    public function __construct(array $configs)
    {
        foreach ($configs as $instance => $config) {
            $this->setInstance($instance, $config);
        }
    }

    private function setInstance(string $instance, array $config)
    {
        $className = "BreakCode\\Connector\\" . $instance;

        if (!class_exists($className)) {
            return;
        }
        $this->instance[$instance] = new $className();
        $this->instance[$instance]->setOptions($config);
    }

    public function __call($method, $param)
    {
        $instance = $param[1] ?? '';
        if (!is_string($instance)) {
            $instance = '';
        }
        $breakCodeKey = array_keys($this->instance);

        if (!in_array($instance, $breakCodeKey)) {
            $instance = '';
        }
        if (!$instance) {
            $instance = $breakCodeKey[0];
        }

        $instance = $this->instance[$instance];

        return $instance->$method(...$param);
    }
}
